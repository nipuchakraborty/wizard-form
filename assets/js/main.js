var stepcounter=1;
var progresser=6.67;







$(function(){
   $( "#datepicker" ).datepicker();
})


/* step 1 update status on change */

$('input#engagement_ring').on('change', function (e) {
   if (e.target.checked) {
      elements.step1.input1 = true
   } else {
      elements.step1.input1 = false
   }
})

$('input#honeymoon').on('change', function (e) {
   if (e.target.checked) {
      elements.step1.input2 = true
   } else {
      elements.step1.input2 = false
   }
})
$('input#venu').on('change', function (e) {
   if (e.target.checked) {
      elements.step1.input3 = true
   } else {
      elements.step1.input3 = false
   }
})
$('input#other').on('change', function (e) {
   if (e.target.checked) {
      elements.step1.input4 = true
   } else {
      elements.step1.input4 = false
   }
});


/* end of step 1 */



/* step 2 updated status on change */

$('input#price_range1').on('change', function (e) {
   if (e.target.checked) {
      elements.step2.input1 = true;
   } else {
      elements.step2.input1 = false;
   }
});

$('input#price_range2').on('change', function (e) {
   if (e.target.checked) {
      elements.step2.input2 = true;
   } else {
      elements.step2.input2 = false;
   }
});
$('input#price_range3').on('change', function (e) {
   if (e.target.checked) {
      elements.step2.input3 = true;
   } else {
      elements.step2.input3 = false;
   }
});
$('input#price_range4').on('change', function (e) {
   if (e.target.checked) {
      elements.step2.input4 = true;
   } else {
      elements.step2.input4 = false;
   }
   console.log(elements.step2)
});
/* end of step 2 update */


/* step 3 update element on keypress */

$('input#first_name').on('keyup', function (e) {
   e.preventDefault();
   elements.step3.input1 = $(this).val().length > 0;
   $(this).css('border','0px');
   $('.message').empty()
});

$('input#last_name').on('keyup', function (e) {
   e.preventDefault();
   elements.step3.input2 = $(this).val().length > 0;
   $(this).css('border','0px');
   $('.message').empty()
});
/* end of step 3 element update */


/* step4 update element on key up*/
$('input#zipcode').on('keyup',function(e){
  e.preventDefault();
  elements.step4=$(this).val().length>0;
  $(this).css('border','0px');
  $('.message').empty()
})
/* step4 end */

/* step5 update element on keyup */
$('input#email').on('keyup',function(e){
  e.preventDefault();
  elements.step5=$(this).val().length>0;
  $(this).css('border','0px');
  $('.message').empty()
});
// end of step-5

// update element on keyup step-6

$('input#phone').on('keyup',function(e){
e.preventDefault();
 elements.step6=$(this).val().length>0;
});
/* end of step 6 status update */

//update element status step 7

$('input#yes_option').on('change',function(e){
   e.preventDefault();
   if(e.target.checked){
      elements.step7.input1=true;
   }
   else{
      elements.step7.input1=false;
   }
});

$('input#no_option').on('change',function(e){
   e.preventDefault();
   if(e.target.checked){
      elements.step7.input2=true;
   }
   else{
      elements.step7.input2=false;
   }
});/* endof step-7 element status update */



/* step-8 element status update */

$('input#poor').on('change',function(e){
   e.preventDefault();
   if(e.target.checked) {
      elements.step8.input1=true;
   }
   else{
      elements.step8.input1=false;
   }
});
$('input#fair').on('change',function(e){
   e.preventDefault();
   if(e.target.checked) {
      elements.step8.input2=true;
   }
   else{
      elements.step8.input2=false;
   }
});
$('input#good').on('change',function(e){
   e.preventDefault();
   if(e.target.checked) {
      elements.step8.input3=true;
   }
   else{
      elements.step8.input3=false;
   }
});
$('input#excellent').on('change',function(e){
   e.preventDefault();
   if(e.target.checked) {
      elements.step8.input4=true;
   }
   else{
      elements.step8.input4=false;
   }
});
/* endof status update step 8 */


/* update element status step-9 */

$('input#anual_income').on('keyup',function(e){
   e.preventDefault();
   elements.step9=$(this).val().length>0
});
//endof element update step9

//step-10 element status update
$('input#employee').on('change',function(e){
if(e.target.checked) elements.step10.input1=true;
else elements.step10.input1=false;
});
$('input#self_employee').on('change',function(e){
   e.preventDefault();
if(e.target.checked) elements.step10.input2=true;
else elements.step10.input2=false;
});
$('input#benefits').on('change',function(e){
   e.preventDefault();
if(e.target.checked) elements.step10.input3=true;
else elements.step10.input3=false;
});
$('input#others').on('change',function(e){
   e.preventDefault();
if(e.target.checked) elements.step10.input4=true;
else elements.step10.input4=false;
});

//step-10 element status update end


//step-11 element status update 
$('input#every_week').on('change',function(e){
   if(e.target.checked) elements.step11.input1=true;
   else elements.step11.input1=false;
   console.log(elements.step11)
});
$('input#every_two_week').on('change',function(e){
   e.preventDefault()
   if(e.target.checked) elements.step11.input2=true;
   else elements.step11.input2=false;
   console.log(elements.step11)
});
$('input#every_month').on('change',function(e){
   e.preventDefault()
   if(e.target.checked) elements.step11.input3=true;
   else elements.step11.input3=false;
   console.log(elements.step11)
});
$('input#something_else').on('change',function(e){
   e.preventDefault();
   if(e.target.checked) elements.step11.input3=true;
   else elements.step11.input4=false;
   console.log(elements.step11)
});
//end of update status step-11

//update status element 12
$('input#answer_yes').on('change',function(e){
   e.preventDefault();
   if(e.target.checked){
      elements.step12.input1=true;
   }
   else{
      elements.step12.input1=false;
   }
});
$('input#answer_no').on('change',function(e){
   e.preventDefault();
   if(e.target.checked){
      elements.step12.input2=true;
   }
   else{
      elements.step12.input2=false;
   }
});

//endof step-12

//step-13 update element
$('input#street_address').on('keyup',function(e){
   e.preventDefault();
   elements.step13=$(this).val().length>0;
});
//endof update Element

//step-14 update-element
$('input#datepicker').on('change',function(e){
   e.preventDefault();
   elements.step14=$(this).val().length>0;
   console.log(elements.step14)
});
//endof element



//step-15 update element status
$('input#password').on('keyup',function(e){
   e.preventDefault();
   elements.step15=$(this).val().length>0;
})
//step15 end update status




const elements = {
   step1: {
      input1: $('.step-1').find('input#engagement_ring').is(":checked"),
      input2: $('.step-1').find('input#honeymoon').is(":checked"),
      input3: $('.step-1').find('input#venu').is(":checked"),
      input4: $('.step-1').find('input#other').is(":checked"),
   },
   step2: {
      input1: $('.step-2').find('input#price_range1').is(":checked"),
      input2: $('.step-2').find('input#price_range2').is(":checked"),
      input3: $('.step-2').find('input#price_range3').is(":checked"),
      input4: $('.step-2').find('input#price_range4').is(":checked"),

   },
   step3: {
      input1: $('.step-3').find('input#first_name').val().length > 0,
      input2: $('.step-3').find('input#last_name').val().length > 0,
   },
   step4: $('.step-4').find('input#zipcode').val().length > 0,
   step5: $('.step-5').find('input#email').val().length > 0,
   step6: $('.step-6').find('input#phone').val().length > 0,
   step7: {
      input1: $('.step-7').find('input#yes_option').is(':checked'),
      input2: $('.step-7').find('input#no_option').is(':checked')
   },
   step8: {
      input1: $('.step-8').find('input#poor').is(':checked'),
      input2: $('.step-8').find('input#fair').is(":checked"),
      input3: $('.step-8').find('input#good').is(":checked"),
      input4: $('.step-8').find('input#excellent').is(":checked"),
   },
   step9: $('.step-9').find('input#anual_income').val().length > 0,
   step10: {
      input1: $('.step-10').find('input#employee').is(':checked'),
      input2: $('.step-10').find('input#self_employee').is(':checked'),
      input3: $('.step-10').find('input#benefits').is(':checked'),
      input4: $('.step-10').find('input#others').is(':checked')
   },
   step11: {
      input1: $('.step-11').find('input#every_week').is(':checked'),
      input2: $('.step-11').find('input#every_two_week').is(':checked'),
      input3: $('.step-11').find('input#every_month').is(':checked'),
      input4: $('.step-11').find('input#something_else').is(':checked'),
   },
   step12: {
      input1: $('.step-12').find('input#answer_yes').is(':checked'),
      input2: $('.step-12').find('input#answer_no').is(':checked'),
   },
   step13: $('.step-13').find('input#street_address').val().length > 0,
   step14: $('.step-14').find('input#date').text().length> 0,
   step15: $('.step-15').find('input#password').val().length > 0,
};



/* validtion check */
$('.step-1').find('.quiz_continueBtn').on('click', function(e) {
   e.preventDefault();
   const {
      input1,
      input2,
      input3,
      input4
   } = elements.step1;
   let message=$('.step-1 .message')
      message.empty();
   if(input1===false && input2===false && input3===false && input4===false){
      message.append("At least one fild required");
   }
   else{
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
  
   
});

$('.step-2').find('.quiz_continueBtn').on('click', function (e) {
   e.preventDefault();
   
   let message=$(".step-2").find('.message');
   
   const {
      input1,
      input2,
      input3,
      input4
   } = elements.step2;
   if(input1===false && input2===false && input3===false && input4===false){
      message.empty();
      message.append('At least one field required');
   }
   else{
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
});



$('.step-3').find('.quiz_continueBtn').on('click',function(){
      let message=$('.step-3').find('.message');
      message.empty();
   const {input1,input2} =elements.step3;
   if(input1==false && input2===false){
      message.append('Field required');
      $('.step-3').find('input').css('border','1px solid red')
   }
   if(input1==true && input2===false){
      message.append('Field required');
      $('.step-3').find('input#last_name').css('border','1px solid red')
   }
   if(input1==false && input2===true){
      message.append('Field required');
      $('.step-3').find('input#first_name').css('border','1px solid red')
   }
   if(input1==true && input2===true){
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
});


$('.step-4').find('.quiz_continueBtn').on('click',function(e){
   e.preventDefault();
   let message=$('.step-4').find('.message');
   if(elements.step4==false){
      message.empty();
      message.append("field is required");
      $('.step-4').find('input').css('border','1px solid red')
   }
   else{
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
});

/* step-5 */

$('.step-5').find('.quiz_continueBtn').on('click',function(e){
   e.preventDefault();
   let message=$('.step-5').find('.message');
   if(elements.step5==false){
      message.empty();
      message.append("field is required");
      $('.step-5').find('input').css('border','1px solid red')
   }
   else{
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
});

//step-6
$('.step-6').find('.quiz_continueBtn').on('click',function(e){
   e.preventDefault();
   let message=$('.step-6').find('.message');
   if(elements.step6==false){
      message.empty();
      message.append("field is required");
      $('.step-6').find('input').css('border','1px solid red')
   }
   else{
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
});



//step-7

$('.step-7').find('.quiz_continueBtn').on('click',function(e){
   e.preventDefault();
   const {input1, input2} =elements.step7;
   let message=$('.step-7').find('.message');
   if(input1==false && input2===false){
      message.empty();
      message.append("At least one field required");
      $('.step-7').find('input').css('border','1px solid red')
   }
   if(input1===true && input2===false){
      alert(input1)
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
   if(input1===false && input2===true){
      alert(input2)
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
});




$('.step-8').find('.quiz_continueBtn').on('click',function(e){
   e.preventDefault();
   const {input1,input2,input3,input4}=elements.step8;
   let message=$('.step-8').find('.message');
   if(input1==false && input2===false && input2==false && input3===false && input4===false){
      message.empty();
      message.append("At least one field required");
      $('.step-8').find('input').css('border','1px solid red')
   }

   else{
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
});


//step-9

$('.step-9').find('.quiz_continueBtn').on('click',function(e){
   e.preventDefault();
   const {step9}=elements;
   let message=$('.step-9').find('.message');
   if(step9==false){
      message.empty();
      message.append("Field Reuired");
      $('.step-9').find('input').css('border','1px solid red')
   }
   
   else{
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
});


//step-10

$('.step-10').find('.quiz_continueBtn').on('click',function(e){
   e.preventDefault();
   const {input1,input2,input3,input4}=elements.step10;
   let message=$('.step-10').find('.message');
   if(input1==false && input2===false && input3===false && input4===false){
      message.empty();
      message.append("At least one field required");
      $('.step-10').find('input').css('border','1px solid red')
   }

   else{
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
});

//step-11

$('.step-11').find('.quiz_continueBtn').on('click',function(e){
   e.preventDefault();
   const {input1,input2,input3,input4}=elements.step11;
   let message=$('.step-11').find('.message');
   if(input1==false && input2===false  && input3===false && input4===false){
      message.empty();
      message.append("At least one field required");
      $('.step-11').find('input').css('border','1px solid red')
   }

   else{
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
});

//step-12

$('.step-12').find('.quiz_continueBtn').on('click',function(e){
   e.preventDefault();
   const {input1,input2}=elements.step12;
   let message=$('.step-12').find('.message');
   if(input1==false && input2===false){
      message.empty();
      message.append("At least one field required");
      $('.step-12').find('input').css('border','1px solid red')
   }

   else{
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
});

//step-13

$('.step-13').find('.quiz_continueBtn').on('click',function(e){
   e.preventDefault();
   const {step13}=elements;
   let message=$('.step-13').find('.message');
   if(step13==false){
      message.empty();
      message.append("At least one field required");
      $('.step-13').find('input').css('border','1px solid red')
   }

   else{
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
});

//step-14


$('.step-14').find('.quiz_continueBtn').on('click',function(e){
   e.preventDefault();
   const {step14}=elements;
   let message=$('.step-13').find('.message');
   if(step14===false){
      message.empty();
      message.append("At least one field required");
      $('.step-14').find('input').css('border','1px solid red')
   }

   else{
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
});



$('.step-15').find('.quiz_continueBtn').on('click',function(e){
   e.preventDefault();
   const {step15}=elements;
   let message=$('.step-15').find('.message');
   if(step15===false){
      message.empty();
      message.append("At least one field required");
      $('.step-15').find('input').css('border','1px solid red')
   }

   else{
      $('.step-'+stepcounter).hide();
      stepcounter+=1;
      if(stepcounter>=16) stepcounter==16;
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width',progresser+"%");
      progresser+=6.67;
      if(progresser>=100) progresser=100;
   }
});


$(function (){
   // var step = 1;
   // var progress = 6.67;
   /* step forward */
   // $(".quiz_continueBtn").on("click", function (e) {
   //    e.preventDefault();
   //    $(".step-" + step).hide();
   //    step += 1;
   //    if (step >= 16) step = 16;
   //    $(".step-" + step).fadeIn();
   //    $('.progress-bar').css('width', progress + "%");
   //    progress = progress + 6.67;
   //    if (progress >= 100) {
   //       progress = 100;
   //    }
   //    //console.log(step)
   //    //console.log(progress)
   // });



   /* step backward */
   $(".quiz_backBtn").on("click", function (e) {
      e.preventDefault();
      $(".step-" + stepcounter).hide();
      stepcounter -= 1;
      progresser -= 6.67;
      if (progresser <= 0) {
         progresser = 0;
         stepcounter = 1
      }
      $(".step-" + stepcounter).fadeIn();
      $('.progress-bar').css('width', progresser + "%");
      
   });

});

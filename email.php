<?php
// Multiple recipients
$to = 'nipuchakraborty86@gmail.com'; // note the comma

// Subject
$subject = "Civilized Technology";

// Message
$message = '
<html>
<head>
  <title>information</title>
</head>
<body>
    <div class="col-sm-12 step-16">
                        <div class="quiz_content_area quiz_result_content">
                            <h1 class="quiz_title">Review Your Answers</h1>
                            <p class="quiz_para_txt">Make sure your answers are accurate. Accuracy of your answers significantly impacts the offers you may receive.</p>

                            <div class="row justify-content-center">
                                
                                <div class="col-sm-6">
                                    <div class="quiz_card_area">
                                        <div class="quiz_show_result">
                                            <table class="quiz_result_table table" id="quize_chart">
                                                <thead>
                                                    <tr>
                                                        <th scope="col"></th>
                                                        <th scope="col"></th>
                                                    </tr>
                                                </thead><!-- end of thead -->

                                                <tbody>
                                                    <tr>
                                                        <td class="quiz_title_txt">Refinance Amount</td>
                                                        <td class="quiz_info_txt">$100,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="quiz_title_txt">Private Student Debt</td>
                                                        <td class="quiz_info_txt">$13,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="quiz_title_txt">Existing Loan Types</td>
                                                        <td class="quiz_info_txt">Both</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="quiz_title_txt">Credit Score</td>
                                                        <td class="quiz_info_txt">Fair</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="quiz_title_txt">Degree Obtained</td>
                                                        <td class="quiz_info_txt">Bachelors</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="quiz_title_txt">School</td>
                                                        <td class="quiz_info_txt">Eastern New Mexico University - Roswell</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="quiz_title_txt">Graduation Date</td>
                                                        <td class="quiz_info_txt">01/01/1991</td>
                                                    </tr>
                                                </tbody><!-- end of tbody -->
                                            </table><!-- end of quiz_result_table -->
                                        </div><!-- end of single_quiz_card -->
                                    </div><!-- end of quiz_card_area -->
                                </div><!-- end of col6 -->

                                <div class="col-sm-6">
                                    <div class="quiz_card_area">
                                        <div class="quiz_show_result">
                                            <table class="quiz_result_table table" id="quiz_table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col"></th>
                                                        <th scope="col"></th>
                                                    </tr>
                                                </thead><!-- end of thead -->

                                                <tbody>
                                                    <tr>
                                                        <td class="quiz_title_txt">Annual Income</td>
                                                        <td class="quiz_info_txt">$123,123</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="quiz_title_txt">Address</td>
                                                        <td class="quiz_info_txt">3304 Rogers Rd., 2, Austin, TX 78758</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="quiz_title_txt">Name</td>
                                                        <td class="quiz_info_txt"> Henry Owens</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="quiz_title_txt">DOB</td>
                                                        <td class="quiz_info_txt">01/01/1991</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="quiz_title_txt">Email</td>
                                                        <td class="quiz_info_txt"> hello@nocreditcampers.com</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="quiz_title_txt">Residency Status</td>
                                                        <td class="quiz_info_txt">U.S. Citizen</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="quiz_title_txt">SSN</td>
                                                        <td class="quiz_info_txt">***-**-1234</td>
                                                    </tr>
                                                </tbody><!-- end of tbody -->
                                            </table><!-- end of quiz_result_table -->
                                        </div><!-- end of single_quiz_card -->
                                    </div><!-- end of quiz_card_area -->
                                </div><!-- end of col6 -->

                            </div><!-- end of row -->

                            <div class="row justify-content-center">
                                <div class="col-sm-8">
                                    <div class="quiz_result_txt_area">
                                        <h3>We take your privacy seriously. By clicking the “Submit and Load Offers” button<br> below, you consent, acknowledge, and agree to the following:</h3>
                                        <ul class="quiz_txt_list">
                                            <li>HapeCash\'s <a href="#">Terms of Use</a>, <a href="#">Privacy Policy</a>, and <a href="#">E-consent Agreement</a> and to receive important notice and other communications electronically.</li>
                                            <li>You are providing express *wirtten* consent to share your information with up to five (5) Network <a href="#">partners</a> and for HypeCash, parties contacting you on behalf of HypeCash, Network partners, or and authorized third party on their behalf to contact you via emal.</li>
                                        </ul><!-- end of quiz_txt_list -->
                                    </div><!-- end of quiz_result_txt_area -->
                                </div><!-- end of col12 -->
                            </div><!--end of row -->

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="quiz_checkbox">
                                        
                                    </div><!-- end of quiz_checkbox -->
                                    <div class="quiz_next">
                                        <button class="quiz_continueBtn">Continue</button>
                                    </div><!-- end of quiz_next -->
                                </div><!-- end of col12 -->
                            </div><!--end of row -->

                        </div><!-- end of quiz_content_area -->

  </div><!-- end of col12 -->
</body>
</html>
';

// To send HTML mail, the Content-type header must be set
$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-type: text/html; charset=iso-8859-1';

// Additional headers
$headers[] = 'To: Mary <mary@example.com>, Kelly <kelly@example.com>';
$headers[] = 'From: Birthday Reminder <'.$_POST['email'].'>';


// Mail it
mail($to, $subject, $message, implode("\r\n", $headers));
?>